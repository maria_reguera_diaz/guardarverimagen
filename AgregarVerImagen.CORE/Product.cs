﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgregarVerImagen.CORE
{
    /// <summary>
    /// Clase de dominio del producto
    /// </summary>
    public class Product
    {
        /// <summary>
        /// Identificador de producto.
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// Nombre del producto.
        /// </summary>
        public string NameProduct { get; set; }

 

        /// <summary>
        /// Listado de Imagenes de un producto.
        /// </summary>
        public List<ImageProduct> ImageProducts { get; set; }


       

    }
}
