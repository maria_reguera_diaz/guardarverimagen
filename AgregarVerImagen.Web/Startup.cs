﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AgregarVerImagen.Web.Startup))]
namespace AgregarVerImagen.Web
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
