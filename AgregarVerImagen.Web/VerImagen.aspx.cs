﻿using AgregarVerImagen.Application;
using AgregarVerImagen.CORE;
using AgregarVerImagen.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AgregarVerImagen.Web
{
    public partial class VerImagen : System.Web.UI.Page
       
    {
        ApplicationDbContext context = null;
        ImageProductManager imageProductManager = null;
        ProductManager productManager = null;
        Product product = null;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
             context = new ApplicationDbContext();
             imageProductManager = new ImageProductManager(context);
             productManager = new ProductManager(context);
            Product product = new Product();
            {
                product.NameProduct = txtNameProduct.Text;
                product.ImageProducts = new List<ImageProduct>();

            };

            productManager.Add(product);
            productManager.Context.SaveChanges();

            //Guardamos la imagen en la base de datos
            if (fuImage.HasFile)
            {
                foreach (var file in fuImage.PostedFiles)
                {
                    imageProductManager = new ImageProductManager(context);
                    String fileExtension = System.IO.Path.GetExtension(file.FileName).ToLower();
                    String[] allowedExtension = { ".png" };
                    if (allowedExtension.Contains(fileExtension))
                    {
                        file.SaveAs(Server.MapPath("~/Catalogue/") + file.FileName);
                    }
                    ImageProduct imageProduct = new ImageProduct()
                    {
                        ImageName = file.FileName,
                        ProductId = product.Id
                    };
                    product.ImageProducts.Add(imageProduct);
                    imageProductManager.Add(imageProduct);
                    imageProduct = null;


                }
                imageProductManager.Context.SaveChanges();
            }
            lblresult.Text = "Producto guardado con exito";

        }

        protected void btnVerImagen_Click(object sender, EventArgs e)
        {
            context = new ApplicationDbContext();
            imageProductManager = new ImageProductManager(context);
            productManager = new ProductManager(context);
            product = new Product();

            imgProductUrl.ImageUrl = "~/ VistaImagen.aspx ? id = " + txtId.Text;
            var imageProduct = imageProductManager.GetAll()
                           .Include(m => m.Product)
                           .Where(m => m.ProductId == product.Id);
            if (imageProduct != null)
            {
                foreach (var imageProducts in imageProduct)
                {
                    imgProductUrl.ImageUrl = "~/Catalogue/";
                   
                }
            }
        }
    }
}
