﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="VerImagen.aspx.cs" Inherits="AgregarVerImagen.Web.VerImagen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div ID="container">
        <div>
            <table border="1">
                <tr>
                    <td><h1>Agregar imagen</h1></td>
                </tr>
                <tr>
                    <td><asp:Label runat="server" Text="Id"></asp:Label></td>
                    <td><asp:TextBox ID="txtId" runat="server"></asp:TextBox> </td>
                </tr>
                 <tr>
                    <td><asp:Label runat="server" Text="Id"></asp:Label></td>
                    <td><asp:TextBox ID="txtIdProduct" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td><asp:Label runat="server" Text="Name"></asp:Label></td>
                    <td><asp:TextBox ID="txtNameProduct" runat="server"></asp:TextBox> </td>
                </tr>
                <tr>
                    <td><asp:Label runat="server" Text="Imagen"></asp:Label></td>
                    <td><asp:FileUpload ID="fuImage" runat="server" /> </td>
                </tr>
                <tr>
                    <td align="center"colspan="2" ><asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" /></td>
                </tr>
                <tr>
                <td colspan="2">
                    <asp:Image ID="imgProductUrl" style="height:300px; width:500px; border:solid;" runat="server" />
                </td>
                    </tr>
                <tr>
                    <td align="center"colspan="2" ><asp:Button ID="btnVerImagen" runat="server" Text="VerImagen" OnClick="btnVerImagen_Click" /></td>
                </tr>
            </table>
            <asp:Label ID="lblresult" runat="server"></asp:Label>

        </div>

    </div>
</asp:Content>
