﻿using AgregarVerImagen.CORE;
using AgregarVerImagen.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgregarVerImagen.Application
{
    /// <summary>
    /// Manager de producto
    /// </summary>
    public class ProductManager : GenericManager<Product>
    {
        /// <summary>
        /// Constructor del manager de producto
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public ProductManager(ApplicationDbContext context) : base(context)
        {

        }
    }
}