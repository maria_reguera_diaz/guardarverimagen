﻿using AgregarVerImagen.CORE;
using AgregarVerImagen.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgregarVerImagen.Application
{
  public class ImageProductManager : GenericManager<ImageProduct>

    {
        public ImageProductManager(ApplicationDbContext context) : base(context)
        {

        }
    }
}
